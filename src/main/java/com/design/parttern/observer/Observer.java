package com.design.parttern.observer;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public abstract class Observer {

    protected Subject subject;

    public abstract void update();
}
