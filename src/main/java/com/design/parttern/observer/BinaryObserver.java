package com.design.parttern.observer;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public class BinaryObserver extends Observer{


    public BinaryObserver(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println( "Binary String: "
                + Integer.toBinaryString( subject.getState() ) );
    }
}
