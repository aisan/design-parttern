package com.design.parttern.service.locator;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public class Service2 implements Service {
    @Override
    public String getName() {
        return "Service2";
    }

    @Override
    public void execute() {
        System.out.println("Executing Service2");
    }
}
