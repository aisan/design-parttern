package com.design.parttern.service.locator;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public interface Service {

    String getName();

    void execute();
}
