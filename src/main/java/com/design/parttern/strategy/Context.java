package com.design.parttern.strategy;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public class Context {

    private Strategy strategy;

    public Context(Strategy strategy){
        this.strategy = strategy;
    }


    public int executeStrategy(int num1, int num2){
        return strategy.doOperation(num1, num2);
    }
}
