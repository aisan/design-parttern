package com.design.parttern.strategy;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public interface Strategy {

    public int doOperation(int num1, int num2);
}
