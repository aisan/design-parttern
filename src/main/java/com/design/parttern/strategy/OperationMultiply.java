package com.design.parttern.strategy;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public class OperationMultiply implements Strategy {
    @Override
    public int doOperation(int num1, int num2) {
        return num1*num2;
    }
}
