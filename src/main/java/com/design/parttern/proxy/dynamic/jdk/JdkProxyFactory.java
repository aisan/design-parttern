package com.design.parttern.proxy.dynamic.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author Qiurz
 * @date 2021/6/14
 */
public class JdkProxyFactory {

    /** 维护一个目标对象 */
    private Object target;

    public JdkProxyFactory(Object object){
        this.target = object;
    }

    /** 为目标对象生成一个代理对象 */
    public Object getInstance(){

        Object proxyObj = Proxy.newProxyInstance(target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),new InvocationHandler(){

                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println("begin proxy...");
                        Object returnValue = method.invoke(target, args);
                        System.out.println("end proxy...");
                        return returnValue;
                    }
                });

        return proxyObj;
    }

}
