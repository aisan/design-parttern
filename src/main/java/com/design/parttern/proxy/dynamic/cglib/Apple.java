package com.design.parttern.proxy.dynamic.cglib;

/**
 * @author Qiurz
 * @date 2021/6/14
 */
public class Apple {

    public void pick(){
        System.out.println("pick apple");
    }
}
