package com.design.parttern.proxy.dynamic.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author Qiurz
 * @date 2021/6/14
 */
public class ProxyFactory implements MethodInterceptor {

    /** 维护一个目标对象 */
    private Object target;

    public ProxyFactory(Object object){
        this.target = object;
    }

    /** 为目标对象生成一个代理对象 */
    public Object getInstance(){

        // 工具类
        Enhancer enhancer = new Enhancer();
        // 设置父类
        enhancer.setSuperclass(target.getClass());
        // 设置回调函数
        enhancer.setCallback(this);
        // 创建子类对象代理
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("开始代理");
        Object proxyObj = method.invoke(target, objects);
        System.out.println("代理结束");
        return proxyObj;
    }
}
