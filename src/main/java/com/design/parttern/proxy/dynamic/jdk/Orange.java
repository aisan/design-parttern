package com.design.parttern.proxy.dynamic.jdk;

/**
 * @author Qiurz
 * @date 2021/6/15
 */
public class Orange implements Fruit {


    @Override
    public void pick() {
        System.out.println("pick orange");
    }
}
