package com.design.parttern.proxy.dynamic.jdk;

/**
 * @author Qiurz
 * @date 2021/6/14
 */
public interface Fruit {

    void pick();
}
