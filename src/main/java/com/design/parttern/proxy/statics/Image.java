package com.design.parttern.proxy.statics;

/**
 * @author Qiurz
 * @date 2021/6/4
 */
public interface Image {

    void display();
}
