package com.design.parttern.adapter;

/**
 * @author Qiurz
 * @date 2021/6/10
 */
public interface MediaPlayer {

    void play(String audioType, String fileName);
}
