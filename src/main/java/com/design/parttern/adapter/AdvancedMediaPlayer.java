package com.design.parttern.adapter;

/**
 * @author Qiurz
 * @date 2021/6/10
 */
public interface AdvancedMediaPlayer {

    void playVlc(String fileName);
    void playMp4(String fileName);
}
