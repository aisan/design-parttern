package com.design.parttern.prototype;

/**
 * @author Qiurz
 * @date 2021/6/10
 */
public class Circle extends ShapePrototype {

    public Circle(){
        type = "Circle";
    }

    @Override
    public void draw() {
        System.out.println("Inside Circle::draw() method.");
    }
}