package com.design.parttern.prototype;

/**
 *  浅拷贝实现 Cloneable
 *  深拷贝是通过实现 Serializable 读取二进制流
 * @author Qiurz
 * @date 2021/6/10
 */
public abstract class ShapePrototype implements Cloneable{


    private String id;

    protected String type;

    abstract void draw();

    @Override
    public Object clone(){
        Object object = null;
        try {
            object = super.clone();
        }catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }


        return object;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

}
