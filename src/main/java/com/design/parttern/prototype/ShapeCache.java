package com.design.parttern.prototype;


import java.util.Hashtable;

/**
 * @author Qiurz
 * @date 2021/6/10
 */
public class ShapeCache {

    private static Hashtable<String,ShapePrototype> shapeMap = new Hashtable<>();


    public static ShapePrototype getShape(String shapeId){
        ShapePrototype cacheShape = shapeMap.get(shapeId);
        return (ShapePrototype) cacheShape.clone();
//        return cacheShape;
    }


    // 对每种形状都运行数据库查询，并创建该形状
    // shapeMap.put(shapeKey, shape);
    // 例如，我们要添加三种形状


    public static void loadShape(){
        Circle circle = new Circle();
        circle.setId("1");
        shapeMap.put(circle.getId(),circle);

        Rectangle rectangle = new Rectangle();
        rectangle.setId("2");
        shapeMap.put(rectangle.getId(),rectangle);

        Square square = new Square();
        square.setId("3");
        shapeMap.put(square.getId(),square);

        System.out.println("shapeMap: "+shapeMap);
    }
}
