package com.design.parttern.prototype;

/**
 * @author Qiurz
 * @date 2021/6/10
 */
public class Rectangle extends ShapePrototype {

    public Rectangle(){
        type = "Rectangle";
    }

    @Override
    void draw() {
        System.out.println("Inside Rectangle::draw() method.");
    }
}
