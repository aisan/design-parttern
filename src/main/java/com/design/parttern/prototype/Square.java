package com.design.parttern.prototype;

/**
 * @author Qiurz
 * @date 2021/6/10
 */
public class Square extends ShapePrototype {

    public Square(){
        type = "Square";
    }

    @Override
    public void draw() {
        System.out.println("Inside Square::draw() method.");
    }
}