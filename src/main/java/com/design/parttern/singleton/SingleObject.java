package com.design.parttern.singleton;

/**
 *  饿汉式
 * @author Qiurz
 * @date 2021/6/4
 */
public class SingleObject {

    // 创建一个对象
    private static SingleObject instance = new SingleObject();

    //构建私有函数
    private SingleObject(){};

    // 获取唯一可用的对象
    public static SingleObject getInstance(){
        return instance;
    }

    public void sendMsg(){
        System.out.println("发送一个消息");
    }
}
