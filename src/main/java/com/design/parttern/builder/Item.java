package com.design.parttern.builder;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public interface Item {

    String name();
    Packing packing();
    float price();
}
