package com.design.parttern.builder;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public class Wrapper implements Packing {
    @Override
    public String pack() {
        return "Wrapper";
    }
}
