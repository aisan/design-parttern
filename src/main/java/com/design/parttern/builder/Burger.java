package com.design.parttern.builder;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public abstract class Burger implements Item {

    @Override
    public Packing packing() {
        return new Wrapper();
    }

    @Override
    public abstract float price();
}
