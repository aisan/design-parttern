package com.design.parttern.builder;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public class VegBurger extends Burger {
    @Override
    public String name() {
        return "Veg Burger";
    }

    @Override
    public float price() {
        return 25.0f;
    }
}
