package com.design.parttern.builder;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public class MealBuilder {

    public Meal prepareVegMeal (){
        Meal meal = new Meal();
        meal.addItem(new VegBurger());
        meal.addItem(new Coke());
        return meal;
    }

}
