package com.design.parttern.builder;

/**
 * @author Qiurz
 * @date 2021/6/5
 */
public class Coke extends ColdDrink {
    @Override
    public String name() {
        return "Coke";
    }

    @Override
    public float price() {
        return 30.0f;
    }
}
