package com.design.parttern.factory.abst;

/**
 * @author Qiurz
 * @date 2021/6/4
 */
public interface Color {

    void fill();
}
