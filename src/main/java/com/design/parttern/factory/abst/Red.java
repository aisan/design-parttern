package com.design.parttern.factory.abst;

/**
 * @author Qiurz
 * @date 2021/6/4
 */
public class Red implements Color {
    @Override
    public void fill() {
        System.out.println("填充红色");
    }
}
