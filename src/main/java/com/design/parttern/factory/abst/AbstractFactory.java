package com.design.parttern.factory.abst;

import com.design.parttern.factory.Shape;

/**
 * @author Qiurz
 * @date 2021/6/4
 */
public abstract class AbstractFactory {

    public abstract Color getColor(String color);
    public abstract Shape getShape(String shape) ;
}
