package com.design.parttern.factory.abst;

/**
 * @author Qiurz
 * @date 2021/6/4
 */
public class Blue implements Color {
    @Override
    public void fill() {
        System.out.println("填充蓝色");
    }
}
