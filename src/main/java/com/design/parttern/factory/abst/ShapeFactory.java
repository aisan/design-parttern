package com.design.parttern.factory.abst;

import com.design.parttern.factory.Circle;
import com.design.parttern.factory.Rectangle;
import com.design.parttern.factory.Shape;
import com.design.parttern.factory.Square;

/**
 * @author Qiurz
 * @date 2021/6/4
 */
public class ShapeFactory extends AbstractFactory {
    @Override
    public Color getColor(String color) {
        return null;
    }

    @Override
    public Shape getShape(String shapeType) {
        if(shapeType == null){
            return null;
        }
        if(shapeType.equalsIgnoreCase("CIRCLE")){
            return new Circle();
        } else if(shapeType.equalsIgnoreCase("RECTANGLE")){
            return new Rectangle();
        } else if(shapeType.equalsIgnoreCase("SQUARE")){
            return new Square();
        }
        return null;
    }
}
