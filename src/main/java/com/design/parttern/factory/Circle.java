package com.design.parttern.factory;

/**
 *  圆圈
 * @author Qiurz
 * @date 2021/6/4
 */
public class Circle implements Shape {

    @Override
    public void draw() {
        System.out.println("绘制一个圆圈");
    }
}
