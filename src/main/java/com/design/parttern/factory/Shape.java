package com.design.parttern.factory;

/**
 *  定义一个形状接口
 * @author Qiurz
 * @date 2021/6/4
 */
public interface Shape {

    /**
     *  绘制动作
     */
    void draw();
}
