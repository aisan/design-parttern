package com.design.parttern.factory;

/**
 *  正方形
 * @author Qiurz
 * @date 2021/6/4
 */
public class Square implements Shape {

    @Override
    public void draw() {
        System.out.println("绘制一个正方形");
    }
}
