package com.design.parttern;

import com.design.parttern.adapter.AudioPlayer;
import com.design.parttern.builder.Meal;
import com.design.parttern.builder.MealBuilder;
import com.design.parttern.factory.Shape;
import com.design.parttern.factory.ShapeFactory;
import com.design.parttern.factory.abst.AbstractFactory;
import com.design.parttern.factory.abst.Color;
import com.design.parttern.factory.abst.FactoryProducer;
import com.design.parttern.observer.BinaryObserver;
import com.design.parttern.observer.HexaObserver;
import com.design.parttern.observer.OctalObserver;
import com.design.parttern.observer.Subject;
import com.design.parttern.prototype.ShapeCache;
import com.design.parttern.prototype.ShapePrototype;
import com.design.parttern.proxy.dynamic.cglib.Apple;
import com.design.parttern.proxy.dynamic.cglib.ProxyFactory;
import com.design.parttern.proxy.dynamic.jdk.Fruit;
import com.design.parttern.proxy.dynamic.jdk.JdkProxyFactory;
import com.design.parttern.proxy.dynamic.jdk.Orange;
import com.design.parttern.proxy.statics.Image;
import com.design.parttern.proxy.statics.ProxyImage;
import com.design.parttern.service.locator.Service;
import com.design.parttern.service.locator.ServiceLocator;
import com.design.parttern.singleton.SingleObject;
import com.design.parttern.strategy.Context;
import com.design.parttern.strategy.OperationAdd;
import com.design.parttern.strategy.OperationMultiply;
import com.design.parttern.strategy.OperationSubtract;
import com.design.parttern.template.Cricket;
import com.design.parttern.template.Football;
import com.design.parttern.template.Game;
import org.junit.Test;

/**
 * https://www.runoob.com/design-pattern/builder-pattern.html
 * 菜鸟教程网站
 *
 */
public class AppTest {


    /**
     * 工厂模式
     */
    @Test
    public void testFactory(){
        ShapeFactory shapeFactory = new ShapeFactory();

        //获取 Circle 的对象，并调用它的 draw 方法
        Shape shape1 = shapeFactory.getShape("CIRCLE");

        //调用 Circle 的 draw 方法
        shape1.draw();

        //获取 Rectangle 的对象，并调用它的 draw 方法
        Shape shape2 = shapeFactory.getShape("RECTANGLE");

        //调用 Rectangle 的 draw 方法
        shape2.draw();

        //获取 Square 的对象，并调用它的 draw 方法
        Shape shape3 = shapeFactory.getShape("SQUARE");

        //调用 Square 的 draw 方法
        shape3.draw();
    }


    /**
     * 抽象工厂模式
     */
    @Test
    public void testAbsFactory(){
        //获取形状工厂
        AbstractFactory shapeFactory = FactoryProducer.getFactory("SHAPE");

        //获取形状为 Circle 的对象
        Shape shape1 = shapeFactory.getShape("CIRCLE");

        //调用 Circle 的 draw 方法
        shape1.draw();

        //获取形状为 Rectangle 的对象
        Shape shape2 = shapeFactory.getShape("RECTANGLE");

        //调用 Rectangle 的 draw 方法
        shape2.draw();

        //获取形状为 Square 的对象
        Shape shape3 = shapeFactory.getShape("SQUARE");

        //调用 Square 的 draw 方法
        shape3.draw();

        //获取颜色工厂
        AbstractFactory colorFactory = FactoryProducer.getFactory("COLOR");

        //获取颜色为 Red 的对象
        Color color1 = colorFactory.getColor("RED");

        //调用 Red 的 fill 方法
        color1.fill();

        //获取颜色为 Green 的对象
        Color color2 = colorFactory.getColor("Green");

        //调用 Green 的 fill 方法
        color2.fill();

        //获取颜色为 Blue 的对象
        Color color3 = colorFactory.getColor("BLUE");

        //调用 Blue 的 fill 方法
        color3.fill();
    }

    /**
     *  单例模式
     *
     *  经验：
     *  一般情况下，不建议使用第 1 种和第 2 种懒汉方式，建议使用第 3 种饿汉方式。
     *  只有在要明确实现 lazy loading 效果时，才会使用第 5 种登记方式。
     *  如果涉及到反序列化创建对象时，可以尝试使用第 6 种枚举方式。
     *  如果有其他特殊的需求，可以考虑使用第 4 种双检锁方式。
     */
    @Test
    public void testSingleton(){
        //SingleObject singleObject = new SingleObject();

        SingleObject singleObject = SingleObject.getInstance();
        singleObject.sendMsg();
        System.out.println(singleObject);
        // com.design.parttern.singleton.SingleObject@2cdf8d8a
        // com.design.parttern.singleton.SingleObject@2cdf8d8a

    }


    /**
     *  代理模式
     */
    @Test
    public void testProxy(){
        Image image = new ProxyImage("test_10mb.jpg");

        // 图像将从磁盘加载
        image.display();
        System.out.println("");
        // 图像不需要从磁盘加载
        image.display();
    }

    /**
     * 动态代理
     */
    @Test
    public void testDynamic(){
        // 目标对象
        Apple apple = new Apple();
        System.out.println(apple.getClass());

        // cglib代理对象
        Apple proxyApple = (Apple) new ProxyFactory(apple).getInstance();
        System.out.println(proxyApple.getClass());
        proxyApple.pick();

        // jdk代理对象
        Orange orange = new Orange();
        System.out.println(orange.getClass());

        Fruit orangeProxy = (Fruit) new JdkProxyFactory(orange).getInstance();
        System.out.println(orangeProxy.getClass());
        orangeProxy.pick();

    }


    /**
     *  策略模式
     */
    @Test
    public void testStrategy(){
        Context context = new Context(new OperationAdd());
        System.out.println("10 + 5 = " + context.executeStrategy(10, 5));

        context = new Context(new OperationSubtract());
        System.out.println("10 - 5 = " + context.executeStrategy(10, 5));

        context = new Context(new OperationMultiply());
        System.out.println("10 * 5 = " + context.executeStrategy(10, 5));
    }

    /**
     *  模板模式
     */
    @Test
    public void testTemplate(){
        Game game = new Cricket();
        game.play();
        System.out.println();
        game = new Football();
        game.play();
    }


    /**
     * 建造者模式
     */
    @Test
    public void testBuilder(){
        MealBuilder mealBuilder = new MealBuilder();

        Meal vegMeal = mealBuilder.prepareVegMeal();
        System.out.println("Veg Meal");
        vegMeal.showItems();
        System.out.println("Total Cost: " +vegMeal.getCost());

    }


    /**
     * 服务定位器模式
     */
    @Test
    public void testServiceLocator(){

        Service service = ServiceLocator.getService("Service1");
        service.execute();
        service = ServiceLocator.getService("Service2");
        service.execute();
        service = ServiceLocator.getService("Service1");
        service.execute();
        service = ServiceLocator.getService("Service2");
        service.execute();

    }


    /**
     *  观察者模式
     */
    @Test
    public void testObserver(){
        Subject subject = new Subject();

        new HexaObserver(subject);
        new OctalObserver(subject);
        new BinaryObserver(subject);

        System.out.println("First state change: 15");
        subject.setState(15);
        System.out.println("Second state change: 10");
        subject.setState(10);
    }


    /**
     *  原型模式
     *  <p>
     *
     *
     *  </p>
     */
    @Test
    public void testPrototype(){

        ShapeCache.loadShape();

        ShapePrototype clonedShape = (ShapePrototype) ShapeCache.getShape("1");
        System.out.println("Shape : " + clonedShape.getType() + "|"+ clonedShape);

        ShapePrototype clonedShape2 = (ShapePrototype) ShapeCache.getShape("2");
        System.out.println("Shape : " + clonedShape2.getType()+ "|"+ clonedShape2);

        ShapePrototype clonedShape3 = (ShapePrototype) ShapeCache.getShape("3");
        System.out.println("Shape : " + clonedShape3.getType()+ "|"+ clonedShape3);

    }


    /**
     *  适配器模式
     */
    @Test
    public void testAdapter(){

        AudioPlayer audioPlayer = new AudioPlayer();

        audioPlayer.play("mp3", "beyond the horizon.mp3");
        audioPlayer.play("mp4", "alone.mp4");
        audioPlayer.play("vlc", "far far away.vlc");
        audioPlayer.play("avi", "mind me.avi");

    }
}
